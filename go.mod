module gitlab.com/gencove/htsutil

go 1.16

require (
	github.com/biogo/hts v1.4.3
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.2.1
	github.com/spf13/viper v1.9.0
	golang.org/x/sys v0.0.0-20211117180635-dee7805ff2e1 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/ini.v1 v1.64.0 // indirect
)
