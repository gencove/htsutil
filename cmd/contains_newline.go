package cmd

import (
	"bufio"
	"bytes"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

// containsNewlineCmd represents the contains-newline command
var containsNewlineCmd = &cobra.Command{
	Use:   "contains-newline",
	Short: "Prints all the lines from a file that contain the character newline '\\n'.",
	Long: `Prints all the lines from a file that contain the character newline '\\n'. If a line
finishes with EOF then it is omitted from the output.`,
	Example: `	$ htsutil misc contains-newline --file impute.vcf`,
	Run: runContainsNewlineCommand,
}

func init() {
	miscCmd.AddCommand(containsNewlineCmd)

	containsNewlineCmd.Flags().String("file", "", "Path to file. Nothing or '-' for stdin.")
}

// dropCR drops a terminal \r from the data.
func dropCR(data []byte) []byte {
	if len(data) > 0 && data[len(data)-1] == '\r' {
		return data[0 : len(data)-1]
	}
	return data
}

// scanLines copied from bufio/scan.go
func scanLines(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := bytes.IndexByte(data, '\n'); i >= 0 {
		// We have a full newline-terminated line.
		return i + 1, dropCR(data[0:i]), nil
	}
	// If we're at EOF, we have a final, non-terminated line.
	//if atEOF {
	//	return len(data), dropCR(data), nil
	//}
	// Request more data.
	return 0, nil, nil
}

func runContainsNewlineCommand(cmd *cobra.Command, args []string) {
	// Read cli flags
	file, _ := cmd.Flags().GetString("file")
	log.Debug("File to read '", file, "'")

	var f *os.File
	var err error

	if file == "" || file == "-" || file == "/dev/stdin" {
		log.Debug("Reading from stdin")
		f = os.Stdin
	} else {
		log.Debug("Reading from local file")
		f, err = os.Open(file)
		if err != nil {
			log.Fatalf("Could not open file %q:\n", err)
		}
		defer f.Close()
	}

	scanner := bufio.NewScanner(f)
	scanner.Split(scanLines)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatalf("Error scanning the file %q:\n", err)
	}
}
