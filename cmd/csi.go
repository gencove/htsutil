package cmd

import (
	"github.com/spf13/cobra"
)

// csiCmd represents the csi command
var csiCmd = &cobra.Command{
	Use:   "csi",
	Short: "Command line tools for using with .csi files.",
	Long:  `Command line tools for using with .csi files.`,
}

func init() {
	RootCmd.AddCommand(csiCmd)
}
