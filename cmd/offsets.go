package cmd

import (
	"compress/gzip"
	"fmt"
	"os"
	"regexp"
	"strconv"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/gencove/htsutil/internal/csi"
)

// offsetsCmd represents the offsets command
var offsetsCmd = &cobra.Command{
	Use:   "offsets",
	Short: "Get start and stop offsets for nearest BGZF block.",
	Long: `Get start byte offset, start virtual offset and stop byte offset for
nearest BGZF block of a VCF file.
The output is of the form <byte-beg>:<virtual-offset-beg>-<byte-end>.
The byte beg and end are used to retrieve only a portion of the VCF file.
The virtual offset is the amount of bytes that should be skipped at
the beginning of the uncompressed portion of the VCF file.`,
	Example: `	$ htsutil csi offsets --file impute.vcf.gz.csi --region X:70000-80000
	1061113512:41045-1061121137`,
	Run: runOffsetCommand,
}

func init() {
	csiCmd.AddCommand(offsetsCmd)

	offsetsCmd.Flags().String("file", "", "Path to csi file.")
	offsetsCmd.Flags().String("region", "", "chr|chr:beg-end")

	offsetsCmd.MarkFlagRequired("region")
}

func runOffsetCommand(cmd *cobra.Command, args []string) {
	// Read cli flags
	file, _ := cmd.Flags().GetString("file")
	log.Debug("File to read '", file, "'")

	region, _ := cmd.Flags().GetString("region")
	pattern := "^(?P<chr>[^:]*?)(:(?P<beg>[0-9]+)-(?P<end>[0-9]+))?$"

	log.Debug("Region should match pattern region: ", region, " pattern: ", pattern)
	p := regexp.MustCompile(pattern)
	if !p.MatchString(region) {
		log.Fatalf("Region format is incorrect. Expected: chr|chr:beg-end\n")
	}

	regions := p.FindStringSubmatch(region)
	log.Debug("Regions matched ", regions)
	chr := regions[1]
	beg, _ := strconv.Atoi(regions[3])
	end, _ := strconv.Atoi(regions[4])

	// Read csi file
	var csiFile *csi.CSIFile
	var f *os.File
	var err error

	if file == "" || file == "-" || file == "/dev/stdin" {
		log.Debug("Reading from stdin")
		f = os.Stdin
	} else {
		log.Debug("Reading from local file")
		f, err = os.Open(file)
		if err != nil {
			log.Fatalf("Could not open file %q:\n", err)
		}
		defer f.Close()
	}
	log.Debug("Reading gzipped file")
	gz, err := gzip.NewReader(f)
	if err != nil {
		log.Fatalf("Could not read file %q:\n", err)
	}
	defer gz.Close()
	csiFile, err = csi.NewCSIFile(gz)
	if err != nil {
		log.Fatalf("Could not read index %q:\n", err)
	}

	begOffset, endOffset, found := csiFile.GetOffsetsForRegion(chr, beg, end)
	if !found {
		log.Fatalf("Region not found\n")
	}

	fmt.Printf("%d:%d-", begOffset.File, begOffset.Block)
	log.Debugf("Output=%d:%d-%d", begOffset.File, begOffset.Block, endOffset.File-1)
	if begOffset.File != endOffset.File {
		fmt.Printf("%d", endOffset.File-1)
	}
	fmt.Print("\n")
}
