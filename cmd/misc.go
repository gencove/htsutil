package cmd

import (
	"github.com/spf13/cobra"
)

// miscCmd represents the misc command
var miscCmd = &cobra.Command{
	Use:   "misc",
	Short: "Miscellaneous command line tools.",
	Long:  `Miscellaneous command line tools.`,
}

func init() {
	RootCmd.AddCommand(miscCmd)
}
