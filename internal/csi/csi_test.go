package csi

import (
	"testing"

	"github.com/biogo/hts/bgzf"
)

func TestGetNextChunkContigTrucatedV2(t *testing.T) {
	/*
		Contig was truncated because we were not detecting next chunk accurately.
		The following output was copied while debugging.
		time="2022-06-29 19:19:22.421219" level=debug msg="Scanning ref=193, bin=1, chunk={{5264125553 34022} {5264323021 7134}}, given={{5264125553 34022} {5264359290 17248}}"
		time="2022-06-29 19:19:22.421235" level=debug msg="Scanning ref=194, bin=0, chunk={{5264359290 17248} {5264383178 0}}, given={{5264125553 34022} {5264359290 17248}}"
	*/
	c := &CSIFile{
		refs: []csiRefIndex{
			{
				bins: []csiBin{
					{},
					{
						bin: 1,
						chunks: []bgzf.Chunk{
							{
								Begin: bgzf.Offset{
									File:  5264125553,
									Block: 34022,
								},
								End: bgzf.Offset{
									File:  5264323021,
									Block: 7134,
								},
							},
						},
					},
				},
			},
			{
				bins: []csiBin{
					{
						bin: 1,
						chunks: []bgzf.Chunk{
							{
								Begin: bgzf.Offset{
									File:  5264359290,
									Block: 17248,
								},
								End: bgzf.Offset{
									File:  5264383178,
									Block: 0,
								},
							},
						},
					},
				},
			},
		},
	}

	nextChunk, found := c.getNextChunk(bgzf.Chunk{
		Begin: bgzf.Offset{
			File:  5264125553,
			Block: 34022,
		},
		End: bgzf.Offset{
			File:  5264359290,
			Block: 17248,
		},
	})

	if !found {
		t.Error("Next chunk not found")
	}

	if nextChunk.Begin.File != 5264359290 || nextChunk.Begin.Block != 17248 || nextChunk.End.File != 5264383178 || nextChunk.End.Block != 0 {
		t.Errorf("Next chunk doesn't match expected output")
	}
}
