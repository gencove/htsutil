package csi

import (
	"io"
	"reflect"
	"unsafe"

	"github.com/biogo/hts/bgzf"
	"github.com/biogo/hts/bgzf/index"
	"github.com/biogo/hts/csi"
	log "github.com/sirupsen/logrus"
)

type csiRefIndex struct {
	bins  []csiBin
	stats *index.ReferenceStats
}

type csiBin struct {
	bin     uint32
	left    bgzf.Offset
	records uint64
	chunks  []bgzf.Chunk
}

// CSIFile csi index file
type CSIFile struct {
	i    *csi.Index
	refs []csiRefIndex
}

// NewCSIFile create new csi file from io stream
func NewCSIFile(r io.Reader) (*CSIFile, error) {
	log.Debug("Creating new CSIFile instance")
	i, err := csi.ReadFrom(r)
	if err != nil {
		log.Debug("Error creating CSIFile ", err)
		return nil, err
	}
	refs := reflect.ValueOf(*i).FieldByName("refs")
	ptr := unsafe.Pointer(refs.Pointer())
	refIndex := (*(*[1 << 28]csiRefIndex)(ptr))[:refs.Len()]
	return &CSIFile{i, refIndex}, nil
}

func vOffset(o bgzf.Offset) int64 {
	return o.File<<16 | int64(o.Block)
}

// getNextChunk returns the immediatly adjacent chunk next to the given chunk, returns false if
// the next chunk is not found
func (c *CSIFile) getNextChunk(givenChunk bgzf.Chunk) (bgzf.Chunk, bool) {
	log.Debug("Get next chunk of ", givenChunk)

	findChunk := func(equalFlag bool) *bgzf.Chunk {
		log.Debug("Searching chunk")
		var nextChunk *bgzf.Chunk
		// We need to find a chunk where the Begin offset is bigger than
		// the End offset of the givenChunk and the Begin offset is the lowest possible
		for _, ref := range c.refs {
			for _, bin := range ref.bins {
				for _, ch := range bin.chunks {
					// When no chunk is found, equalFlag is set to true. There is an edge case when the offsets
					// are the same that is valid only if the next chunk cannot be found in the first attempt
					isLastChance := (equalFlag && ch.Begin.File == givenChunk.End.File)
					// Chunks are sorted by begin offset inside the bin
					if ch.End.File > givenChunk.End.File &&
						(ch.Begin.File > givenChunk.End.File || isLastChance) &&
						(nextChunk == nil || vOffset(ch.Begin) < vOffset(nextChunk.Begin)) {
						nextChunk = &ch
						break
					}
				}
			}
		}
		return nextChunk
	}

	nextChunk := findChunk(false)
	if nextChunk == nil {
		// Try one more time
		nextChunk = findChunk(true)
		if nextChunk == nil {
			log.Debug("Next chunk not found for ", givenChunk)
			return bgzf.Chunk{}, false
		}
	}
	log.Debug("Next chunk found ", *nextChunk)
	return *nextChunk, true
}

// GetOffsetsForReference returns start and end offsets for the given reference, returns false
// if the reference doesn't exist
func (c *CSIFile) GetOffsetsForReference(rid int) (bgzf.Offset, bgzf.Offset, bool) {
	log.Debug("Get offsets for reference genome ", rid)
	if rid >= len(c.refs) {
		log.Debug("Reference genome not found rid: ", rid, " len(c.refs): ", len(c.refs))
		return bgzf.Offset{}, bgzf.Offset{}, false
	}

	log.Debug("Offsets for reference genome found beg: ", c.refs[rid].stats.Chunk.Begin, " end: ", c.refs[rid].stats.Chunk.End)
	last_chunk := c.refs[rid].stats.Chunk
	last_offset := last_chunk.End
	if next_chunk, ok := c.getNextChunk(last_chunk); ok {
		last_offset = next_chunk.End
	}
	return c.refs[rid].stats.Chunk.Begin, last_offset, true
}

// Auxilliary returns Auxilliary data as a string slice
func (c *CSIFile) Auxilliary() []string {
	log.Debug("Read auxilliary data to string slice ", c.i.Auxilliary)
	out := []string{}
	current := ""
	for _, b := range c.i.Auxilliary {
		if b == 0 && current != "" {
			out = append(out, current)
			current = ""
		} else if b != 0 {
			current += string(b)
		}
	}
	log.Debug("Auxilliary data ", out)
	return out
}

// MapChrRef returns a map of chr to ref number
func (c *CSIFile) MapChrRef() map[string]int {
	var hash_pos int
	auxData := c.Auxilliary()
	out := make(map[string]int)
	// Skip all strings until hash ("#") is found, then
	// skip one string and start counting reference genomes
	for i, data := range auxData {
		if data == "#" {
			hash_pos = i
			break
		}
	}
	for i := hash_pos + 2; i < len(auxData); i++ {
		out[auxData[i]] = i - hash_pos - 2
	}
	log.Debug("Map from chr to reference genome id/pos ", out)
	return out
}

// GetOffsetsForRegion returns start and end virtual offsets for a given region,
// returns false if region is not found.
func (c *CSIFile) GetOffsetsForRegion(chr string, beg, end int) (bgzf.Offset, bgzf.Offset, bool) {
	chrIndex := c.MapChrRef()

	log.Debug("Get offsets for region chr: ", chr, " beg: ", beg, " end: ", end)

	rid, found := chrIndex[chr]
	if !found {
		log.Debug("Reference genome not found for chr: ", chr)
		return bgzf.Offset{}, bgzf.Offset{}, false
	}

	if beg == 0 && end == 0 {
		return c.GetOffsetsForReference(rid)
	}

	chunks := c.i.Chunks(rid, beg, end)
	if len(chunks) == 0 {
		log.Debug("Chunks for region not found")
		return bgzf.Offset{}, bgzf.Offset{}, false
	}

	begChunk := chunks[0]
	endChunk := chunks[len(chunks)-1]

	// TODO: make sure that we are retrieving more chunks that needed
	nextChunk, found := c.getNextChunk(endChunk)
	if !found {
		nextChunk = endChunk
	}

	log.Debug("Offsets for given region found beg: ", begChunk.Begin, " end: ", nextChunk.End)

	return begChunk.Begin, nextChunk.End, true
}
