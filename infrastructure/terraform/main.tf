terraform {
  required_version = ">= 1.1.6"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.2.0"
    }
  }

  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "gencove"

    workspaces {
      name = "htsutil"
    }
  }
}

data "aws_region" "current" {
}

data "aws_caller_identity" "current" {
}

# Main configuration
locals {
  region             = "us-east-1"
  project            = "htsutil"
  public_bucket_name = "${local.project}-public"

  # Poor man's assertions:
  # - make sure the caller is in the same region
  assert_dict   = {}
  assert_region = local.region == data.aws_region.current.name ? 1 : local.assert_dict["incorrect AWS region in caller"]
}

provider "aws" {
  region = local.region
}

resource "aws_s3_bucket" "public" {
  bucket = local.public_bucket_name

  tags = {
    Bucket    = local.public_bucket_name
    Name      = local.public_bucket_name
    Project   = local.project
    Env       = local.project
    ManagedBy = "terraform"
  }
}

resource "aws_s3_bucket_policy" "public_policy" {
  bucket = aws_s3_bucket.public.id
  policy = data.aws_iam_policy_document.public_policy.json
}


data "aws_iam_policy_document" "public_policy" {
  statement {
    sid    = "S3ReadPublic"
    effect = "Allow"
    principals {
      type        = "*"
      identifiers = ["*"]
    }
    actions = [
      "s3:ListBucket",
      "s3:GetObject",
    ]
    resources = [
      "arn:aws:s3:::${local.public_bucket_name}",
      "arn:aws:s3:::${local.public_bucket_name}/*",
    ]
  }
}
