package main

import (
	"fmt"

	"github.com/kelseyhightower/envconfig"
	log "github.com/sirupsen/logrus"
	"gitlab.com/gencove/htsutil/cmd"

	_ "embed"
)

//go:embed version/A-major
var major string

//go:embed version/B-minor
var minor string

//go:embed version/C-patch
var patch string

type Specification struct {
	Log string `default:"INFO"`
}

func main() {
	s := Specification{}
	if err := envconfig.Process("gencove", &s); err != nil {
		log.Fatal(err)
	}

	switch s.Log {
	case "DEBUG":
		log.SetFormatter(&log.TextFormatter{
			DisableColors:   true,
			TimestampFormat: "2006-01-02 15:04:05.000000",
			FullTimestamp:   true,
		})
		log.SetLevel(log.DebugLevel)
	default:
		log.SetFormatter(&log.TextFormatter{
			DisableLevelTruncation: true,
			DisableTimestamp:       true,
		})
		log.SetLevel(log.InfoLevel)
	}

	cmd.RootCmd.Version = fmt.Sprintf("%s.%s.%s", major, minor, patch)

	cmd.Execute()
}
