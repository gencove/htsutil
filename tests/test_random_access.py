from tests.util import assert_output_matches_bcftools


def test_random_access_region(sample_name, region):
    assert_output_matches_bcftools(
        "gencove-sbir",
        f"sequence/experiment-A/{sample_name}.vcf.gz",
        f"sequence/experiment-A/{sample_name}.vcf.gz.csi",
        region,
    )


def test_random_access_chrom(sample_name, chrom):
    assert_output_matches_bcftools(
        "gencove-sbir",
        f"sequence/experiment-A/{sample_name}.vcf.gz",
        f"sequence/experiment-A/{sample_name}.vcf.gz.csi",
        chrom,
    )
