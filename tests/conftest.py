import json
import random

import boto3
from botocore import UNSIGNED
from botocore.config import Config


def pytest_addoption(parser):
    parser.addoption(
        "--file_index",
        action="append",
        default=[],
        help="file index",
    )


def random_region(limits, min_distance=20_000_000):
    """Generates a random region within the provided `limits` that is at least `min_distance` wide.

    Args:
        limits (List): Start and end limits for positions.
        min_distance (int, optional): Minimum distance between the start and end positions of the region. Defaults to 20_000_000.

    Returns:
        str: Region as commonly expressed by bcftools: "beg-end".
    """
    beg, end = limits
    assert end - beg > min_distance
    random_beg = random.randrange(beg, end + 1 - min_distance)
    random_end = random_beg + min_distance
    if random_end < end:
        random_end += random.randrange(end + 1 - random_end)
    return f"{random_beg}-{random_end}"


def pytest_generate_tests(metafunc):
    if "sample_name" in metafunc.fixturenames:
        s3 = boto3.client("s3", config=Config(signature_version=UNSIGNED))
        bucket = "htsutil-public"
        ranges_prefix = "random_access/"
        ranges_objects = s3.list_objects_v2(Bucket=bucket, Prefix=ranges_prefix)
        assert ranges_objects["KeyCount"] == 50

        file_index = int(metafunc.config.getoption("file_index")[0])
        regions_key = ranges_objects["Contents"][file_index]["Key"]
        regions_range = json.loads(
            s3.get_object(
                Bucket=bucket,
                Key=regions_key,
            )["Body"].read()
        )
        _, region_filename = regions_key.split("/")
        sample_name = region_filename.replace("_region_range.json", "")

        if "region" in metafunc.fixturenames:
            regions = [
                f"{ch}:{random_region(region_range)}"
                for ch, region_range in regions_range.items()
            ]

            metafunc.parametrize("sample_name", [sample_name])
            metafunc.parametrize("region", regions)
        elif "chrom" in metafunc.fixturenames:
            metafunc.parametrize("sample_name", [sample_name])
            metafunc.parametrize("chrom", [ch for ch, _ in regions_range.items()])
