import pytest
import sh

from tests.util import assert_output_matches_bcftools


def test_beginning_of_file():
    assert_output_matches_bcftools(
        "htsutil-public",
        "check_outputs/impute.vcf.gz",
        "check_outputs/impute.vcf.gz.csi",
        "1:10177-10200",
    )


def test_end_of_file():
    assert_output_matches_bcftools(
        "htsutil-public",
        "check_outputs/impute.vcf.gz",
        "check_outputs/impute.vcf.gz.csi",
        "X:155260475-155260478",
    )


def test_start_of_chromosome():
    assert_output_matches_bcftools(
        "htsutil-public",
        "check_outputs/impute.vcf.gz",
        "check_outputs/impute.vcf.gz.csi",
        "11:61395-61400",
    )


def test_end_of_chromosome():
    assert_output_matches_bcftools(
        "htsutil-public",
        "check_outputs/impute.vcf.gz",
        "check_outputs/impute.vcf.gz.csi",
        "11:134946452-134946460",
    )


def test_entire_chromosome():
    # Standard test to check if we can read an entire chromosome
    assert_output_matches_bcftools(
        "htsutil-public",
        "check_outputs/impute.vcf.gz",
        "check_outputs/impute.vcf.gz.csi",
        "11",
    )


def test_entire_chromosome_truncated_bug():
    # Test added to avoid a regression on a bug detected by an user
    assert_output_matches_bcftools(
        "htsutil-public",
        "check_outputs/NA12892.gvcf.gz",
        "check_outputs/NA12892.gvcf.gz.csi",
        "chrY",
    )


def test_ignore_lines_without_newline():
    assert_output_matches_bcftools(
        "gencove-sbir",
        "sequence/experiment-A/NA20298-0-1-1-0.vcf.gz",
        "sequence/experiment-A/NA20298-0-1-1-0.vcf.gz.csi",
        "3:172290186-172290760",
    )


def test_same_number_of_lines():
    assert_output_matches_bcftools(
        "gencove-sbir",
        "sequence/experiment-A/NA20513-0-1-1-0.vcf.gz",
        "sequence/experiment-A/NA20513-0-1-1-0.vcf.gz.csi",
        "10:29844735-31521815",
    )


def test_line_spans_two_bgzf_blocks():
    assert_output_matches_bcftools(
        "gencove-sbir",
        "sequence/experiment-A/NA20298-0-1-1-0.vcf.gz",
        "sequence/experiment-A/NA20298-0-1-1-0.vcf.gz.csi",
        "3:172318881-172318881",
    )


def test_s3_file_does_not_exists():
    with pytest.raises(sh.ErrorReturnCode_255):
        assert_output_matches_bcftools(
            "htsutil-public",
            "none",
            "none",
            "1:10000-20000",
        )


def test_multiallelics_more_than_one_record_for_pos():
    assert_output_matches_bcftools(
        "gencove-sbir",
        "sequence/experiment-A/NA20298-0-1-1-0.vcf.gz",
        "sequence/experiment-A/NA20298-0-1-1-0.vcf.gz.csi",
        "3:172340863-172340863",
        expected_body_length=2,
    )
