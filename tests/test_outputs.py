import os
from io import StringIO

import sh


def test_end_byte_expected():
    """First bug reported in this MR https://gitlab.com/gencove/htsutil/-/merge_requests/1"""
    out = StringIO()
    err = StringIO()
    sh.htsutil(
        sh.aws(
            "s3",
            "cp",
            "--no-sign-request",
            "s3://htsutil-public/check_outputs/impute.vcf.gz.csi",
            "-",
            _piped=True,
        ),
        "csi",
        "offsets",
        "--region",
        "1:100649-200000",
        _out=out,
        _err=err,
    )
    assert "" == err.getvalue()
    assert "12736:1433-24782" in out.getvalue()


def test_output_cuts_off_last_line():
    """Second bug reported in this MR https://gitlab.com/gencove/htsutil/-/merge_requests/1"""
    out = StringIO()
    err = StringIO()
    sh.tail(
        sh.bcftools(
            sh.bash(
                "./examples/download_vcf_portion.sh",
                "htsutil-public",
                "check_outputs/impute.vcf.gz",
                "htsutil-public",
                "check_outputs/impute.vcf.gz.csi",
                "1:100649-200000",
                _piped=True,
                _env={**os.environ.copy(), "GENCOVE_AWS_NO_SIGN_REQUEST": "1"},
            ),
            "view",
            _piped=True,
        ),
        "-1",
        _out=out,
        _err=err,
    )
    assert "" == err.getvalue()

    assert (
        "1\t174799\trs544087642\tCA\tC\t.\tPASS\t.\tGT:RC:AC:GP:DS\t0/0:0:0:1,1e-10,1e-10:3e-10"
        == out.getvalue().strip()
    )
