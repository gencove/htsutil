import tempfile
import uuid
from io import StringIO

import pytest
import sh


def test_file_not_exists():
    with pytest.raises(sh.ErrorReturnCode_1):
        out = StringIO()
        err = StringIO()
        sh.htsutil(
            "csi",
            "offsets",
            "--file",
            f"/tmp/{uuid.uuid4()}",
            "--region",
            "1:100-200",
            _out=out,
            _err=err,
        )
    assert "" == out.getvalue()
    assert "no such file or directory" in err.getvalue()


def test_region_format():
    with pytest.raises(sh.ErrorReturnCode_1), tempfile.NamedTemporaryFile() as f:
        out = StringIO()
        err = StringIO()
        sh.htsutil(
            "csi",
            "offsets",
            "--file",
            f.name,
            "--region",
            "a:100-",
            _out=out,
            _err=err,
        )
    assert "" == out.getvalue()
    assert "Region format is incorrect. Expected: chr|chr:beg-end" in err.getvalue()


def test_region_format_alternative():
    with pytest.raises(sh.ErrorReturnCode_1), tempfile.NamedTemporaryFile() as f:
        out = StringIO()
        err = StringIO()
        sh.htsutil(
            "csi",
            "offsets",
            "--file",
            f.name,
            "--region",
            "a:b-c",
            _out=out,
            _err=err,
        )
    assert "" == out.getvalue()
    assert "Region format is incorrect. Expected: chr|chr:beg-end" in err.getvalue()


def test_region_not_exists():
    with pytest.raises(sh.ErrorReturnCode_1):
        out = StringIO()
        err = StringIO()
        sh.htsutil(
            sh.aws(
                "s3",
                "cp",
                "--no-sign-request",
                "s3://htsutil-public/check_outputs/impute.vcf.gz.csi",
                "-",
                _piped=True,
            ),
            "csi",
            "offsets",
            "--region",
            "none:1-10",
            _out=out,
            _err=err,
        )
    assert "" == out.getvalue()
    assert "Region not found" in err.getvalue()
