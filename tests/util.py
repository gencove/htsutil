import os
from io import BytesIO, StringIO

import backoff
import sh


def compare_vcf_files(vcf_1: BytesIO, vcf_2: BytesIO, expected_body_length: int = None):
    """Compare vcf files bodies.

    Args:
        vcf_1 (BytesIO): VCF file bytes.
        vcf_2 (BytesIO): VCF file bytes.
        expected_body_length (Optional[int]): Expected body length.
    """
    vcf_lines_1 = vcf_1.getvalue().splitlines()
    vcf_lines_2 = vcf_2.getvalue().splitlines()

    # Remove header lines
    for (i, l) in enumerate(vcf_lines_1):
        if not l.startswith(b"#"):
            vcf_body_begin_1 = i
            break
    for (i, l) in enumerate(vcf_lines_2):
        if not l.startswith(b"#"):
            vcf_body_begin_2 = i
            break

    # Compare body lengths
    vcf_body_len_1 = len(vcf_lines_1) - vcf_body_begin_1
    vcf_body_len_2 = len(vcf_lines_2) - vcf_body_begin_2
    assert vcf_body_len_1 == vcf_body_len_2, "VCF files length should match"

    if expected_body_length is not None:
        assert (
            vcf_body_len_1 == expected_body_length
        ), f"Unexpected VCF file body length"

    # Compare bodies
    for i in range(vcf_body_len_1):
        assert (
            vcf_lines_1[vcf_body_begin_1 + i] == vcf_lines_2[vcf_body_begin_2 + i]
        ), "VCF files lines should match"


@backoff.on_exception(backoff.expo, sh.ErrorReturnCode, max_tries=5, jitter=None)
def bcftools_view(file_and_index: str, region: str):
    out_bcftools = BytesIO()
    err_bcftools = StringIO()

    sh.bcftools(
        "view",
        file_and_index,
        "-o",
        "-",
        "-r",
        region,
        "-t",
        region,
        _out=out_bcftools,
        _err=err_bcftools,
    )

    return [out_bcftools, err_bcftools]


def assert_output_matches_bcftools(
    bucket: str,
    vcf_key: str,
    csi_key: str,
    region: str,
    expected_body_length: int = None,
):
    """Checks script output against bcftools.

    Args:
        bucket (str): Bucket where files are located.
        vcf_key (str): Key where vcf file is located. Must be public.
        csi_key (str): Key where csi file is located. Must be public.
        region (str): Region to scan.
        expected_body_length (Optional[int]): Expected body length.

    Raises:
        AssertionError: When the outputs differ.
    """

    out_htsutil = BytesIO()
    err_htsutil = StringIO()
    sh.bcftools(
        sh.bash(
            "./examples/download_vcf_portion.sh",
            bucket,
            vcf_key,
            bucket,
            csi_key,
            region,
            _piped=True,
            _env={**os.environ.copy(), "GENCOVE_AWS_NO_SIGN_REQUEST": "1"},
        ),
        "view",
        "-",
        "-o",
        "-",
        "-t",
        region,
        _out=out_htsutil,
        _err=err_htsutil,
    )
    assert "" == err_htsutil.getvalue()

    s3_url_vcf = f"https://{bucket}.s3.amazonaws.com/{vcf_key}"
    s3_url_csi = f"https://{bucket}.s3.amazonaws.com/{csi_key}"

    out_bcftools, err_bcftools = bcftools_view(
        f"{s3_url_vcf}##idx##{s3_url_csi}", region
    )
    assert "" == err_bcftools.getvalue()

    compare_vcf_files(out_bcftools, out_htsutil, expected_body_length)
