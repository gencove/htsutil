#!/bin/bash

set -eo pipefail &&\

# Prepare binary
make clean && make &&\

# Add htsutil binary to path
export PATH="$(pwd):$PATH" &&\

# Run tests
export FILE_INDEX="${CI_NODE_INDEX:="1"}" &&\
python3 -m pytest -vv --file_index="$(($FILE_INDEX - 1))" ./tests/test_random_access.py
