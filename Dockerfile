FROM golang:1.16

WORKDIR /scratch

RUN apt-get update &&\
    apt-get install -y awscli python3-pip samtools tabix liblzma-dev libbz2-dev libcurl4-openssl-dev &&\
    wget https://github.com/samtools/bcftools/releases/download/1.15/bcftools-1.15.tar.bz2 &&\
    wget https://github.com/samtools/htslib/releases/download/1.15/htslib-1.15.tar.bz2 &&\
    tar -xf bcftools-1.15.tar.bz2 &&\
    tar -xf htslib-1.15.tar.bz2 &&\
    mv htslib-1.15 htslib &&\
    cd bcftools-1.15 &&\
    make &&\
    cp bcftools /usr/bin/bcftools &&\
    chmod +x /usr/bin/bcftools

COPY requirements-test.txt /scratch

RUN pip3 install -r requirements-test.txt

