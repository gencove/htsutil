# htsutil

Command line tools with useful utilities for dealing with hts files.

The motivation for creating this tool was mainly accessing subsets of [variant call format (VCF)](https://samtools.github.io/hts-specs/VCFv4.2.pdf) files from remote locations like `https://` or `s3://`.

With `htsutil`, a [CSI index file](http://samtools.github.io/hts-specs/CSIv1.pdf) can be read to fetch the byte offsets for a genomic region in the VCF file. This can then be combined with existing and robust tools to retrieve only that portion of the VCF file from a remote location (like `https://` or `s3://`). There is a [reference implementation](#reference-implementations) in this repo.

NB: [bcftools](https://samtools.github.io/bcftools/bcftools.html) already provides this functionality, but unfortunately has unpredictable (and sometimes silent) failure modes when using remote locations like `https://` or `s3://`.

## Releases
Download binary executables for Mac and Linux from [Gitlab Release page for this project](https://gitlab.com/gencove/htsutil/-/releases).

## How to use

```
$ ./htsutil
htsutil is a CLI tool that contains utilities for dealing with htsfiles.
For example: .csi, .tbi, .fai and others.

Usage:
  htsutil [command]

Available Commands:
  completion  generate the autocompletion script for the specified shell
  csi         Command line tools for using with .csi files.
  help        Help about any command

Flags:
      --config string   config file (default is $HOME/.htsutil.yaml)
  -h, --help            help for htsutil
  -t, --toggle          Help message for toggle

Use "htsutil [command] --help" for more information about a command.
```

### Debug mode

To run in debug mode set the env variable `GENCOVE_LOG` to `DEBUG`, for example:

```bash
$ GENCOVE_LOG=DEBUG htsutil csi offsets --region $REGION --file $FILE
```

## Reference implementations

Look under the folder [examples](/examples/) to see reference implementations.

For example:

  - [`download_vcf_portion.sh`](/examples/download_vcf_portion.sh): Uses htsutil to download a portion of a VCF file using a csi index, takes as an input the location of the vcf file, csi file and region of interest.

## Build from source code

### Dependencies
- Go 1.16 or later
- Make

#### Install go MacOS
```
$ brew install go
```

#### Install go Ubuntu/Debian
```
$ sudo apt install golang-go
```

### Clone the repo
```
$ git clone https://gitlab.com/gencove/htsutil.git
```

### Build using make
```
$ make clean
$ make build
```

## Run tests locally

Test are implemented with pytest. For testing we use python version `3.9`.

First install dependencies executing:

```bash
$ pip install -r requirements-test.txt
```

If you update `requirements-test.txt` remember to also update the [docker image for the ci](#publish-new-ci-image).

In order to run tests successfully you also need to have the following installed in your system:

- bcftools
- samtools
- tabix
- awscli
- go >= 1.16
- make


### Running tests

For running "basic" tests that take care of edge cases, error handling and correct output:

```bash
$ ./run_tests.sh
```

For running random access tests (for 1 file):

```bash
$ FILE_INDEX=10 ./run_random_access_tests.sh
```

Where `FILE_INDEX` can be any number between 1 and 50.

The random access tests only run on `main` branch on CI. They run for 50 files in parallel and scan a random region for every chromosome in a vcf file, the output of the tool is compared against the output of bcftools for the same region.


## Publish new CI image

Authenticate to the Container Registry by using your GitLab username and password. If you have Two-Factor Authentication enabled, use a Personal Access Token instead of a password.

```
$ docker login registry.gitlab.com
```

You can add an image to this registry with the following commands:

```
$ docker build -t registry.gitlab.com/gencove/htsutil .
$ docker push registry.gitlab.com/gencove/htsutil
```


## Release process

1. Check for the current version by running `version-01-upgrade.sh print`
2. Make a new branch titled version/X-Y-Z
3. Run `version-01-upgrade.sh` in that branch with an argument `major`, `minor` or `patch`
4. Create a merge request to main
5. Once it is merged, create a merge request of main to prod
