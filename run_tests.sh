#!/bin/bash

set -eo pipefail &&\

# Prepare binary
make clean && make &&\

# Add htsutil binary to path
export PATH="$(pwd):$PATH" &&\

# Run go tests
go test ./... &&\

# Run tests
python3 -m pytest -vv \
    ./tests/test_error_handling.py \
    ./tests/test_outputs.py \
    ./tests/test_edge_cases.py
